# E2E Studios Extensions #

This is an E2E Studios custom plugin to enable features such as header & footer integrations, footfall analytics and other helpful additions.

## Installation ##

1. Click on the `Download ZIP` button at the right to download the plugin.
2. Go to Plugins > Add New in your WordPress admin. Click on `Upload Plugin` and browse for the zip file.
3. Activate the plugin.

## Usage ##

1. Stuff
