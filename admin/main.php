<?php

$key = get_option('e2es_settings');

add_action('admin_menu', 'e2es_menu');


function add_subs_new($sub) {
    add_submenu_page(
        $sub['slug'],
        $sub['page_title'],
        $sub['menu_title'],
        $sub['capability'],
        $sub['menu_slug'],
        function() use ($sub) {
        require_once (plugin_dir_path(__FILE__) . 'view/'.$sub['menu_slug'].'.php');
    });
}

function codemirror_enqueue_scripts($hook) {
    $cm_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
    wp_localize_script('jquery', 'cm_settings', $cm_settings);

    wp_enqueue_script('wp-theme-plugin-editor');
    wp_enqueue_style('wp-codemirror');
}

function e2es_menu()
{
    add_menu_page('E2ES Settings', 'E2ES Settings', 'manage_options', 'e2es',function() {
        require_once (plugin_dir_path(__FILE__) . 'view/settings/index.php');
        wp_enqueue_script('custom-js', plugins_url('/assets/js/custom.js', __FILE__));

        wp_register_script( 'chartjs', 'https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js', null, null, true );
        wp_enqueue_script('chartjs');
        
        wp_enqueue_style('custom-css', plugins_url('/assets/css/custom.css', __FILE__));


        $cm_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
        wp_localize_script('jquery', 'cm_settings', $cm_settings);

        wp_enqueue_script('wp-theme-plugin-editor');
        wp_enqueue_style('wp-codemirror');
        
    },'data:image/svg+xml;base64,' . base64_encode('<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">  <image id="image0" width="32" height="32" x="0" y="0"
    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAn1BMVEUAAAAXorgXorgXorgX
orgWorcWorcXorgXorgXorgWorcXorgXorgXorgXorgXobgXorgWobcXorgXorgXorgXorgXorgW
obcXorgXorgXorgXorgWorcXorgXorgXorgXorgXobgWorcXobgXorgXorgXorgXorgXorgXorgX
orgXorgXobgWorcXorgXorgXorgXorgXorgXorj////rwg6HAAAAM3RSTlMAgoh28sLudM4Q/MYm
9sTCwKxoFN4SBBYK/OroqESQAvp4amJgShYOWEzM5h7oRuDWmCqI4uAqAAAAAWJLR0Q0qbHp/QAA
AAd0SU1FB+YCFwITNhYWbhwAAACdSURBVDjLtdPZDoIwEIXhAVGpuIKgpSouKKLi+v7vJmkTrzoz
F8h//SXNmaQAjXNcsg54H7Iu9GjQZ4AvSDAIhqDBaDyxNJ2FERgwjxNbZqYGgrjDn8BiiaaBTBVW
wF1yxYE1DeQmM0Ba26pdYlak+4Ol/Hj6zSzavyT3hMrPWCV3qAsHXAZcbzSo7gCPCks9X+96hYjR
iqz5z677AkjtZw004xkXAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIyLTAyLTIzVDAyOjE5OjU0KzAz
OjAwWFFJbgAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMi0wMi0yM1QwMjoxOTo1NCswMzowMCkM8dIA
AAAASUVORK5CYII=" />
</svg>'));
    add_action('admin_enqueue_scripts', 'codemirror_enqueue_scripts');

    $subMenu = [
        [
            'slug' => 'e2es',
            'page_title' => 'Dashboard',
            'menu_title' => 'Dashboard',
            'menu_slug' => 'settings/e2es&tab=dashboard'
        ]
    ];

    $subMenu = [
        [
            'slug' => 'e2es',
            'page_title' => 'Dashboard',
            'menu_title' => 'Dashboard',
            'capability' => 'manage_options',
            'menu_slug' => 'dashboard'
        ]
    ];


    foreach($subMenu as $sub) {
        add_subs_new($sub);
    }

}




add_action('admin_init', 'e2es_settings_init');
function e2es_settings_init()
{
    register_setting('e2esPlugin', 'e2es_settings');
    register_setting('e2esPluginFootfall', 'e2es_footfall_settings');
    register_setting( 'e2esPluginOverrides', 'e2es_header', 'trim' );
    register_setting( 'e2esPluginOverrides', 'e2es_footer', 'trim' );
    register_setting( 'e2esPluginOverrides', 'e2es_body', 'trim' );

    $key = get_option('e2es_settings');
if($key !== '') {
    if($key['e2es_has_acf_options'] && function_exists('acf_add_options_page')) {
        acf_add_options_page();
    } 

    if($key['e2es_has_svg_upload']) {
        add_filter(
	'upload_mimes',
	function ( $upload_mimes ) {
		// By default, only administrator users are allowed to add SVGs.
		// To enable more user types edit or comment the lines below but beware of
		// the security risks if you allow any user to upload SVG files.
		if ( ! current_user_can( 'administrator' ) ) {
			return $upload_mimes;
		}

		$upload_mimes['svg']  = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';

		return $upload_mimes;
	}
);

add_filter(
	'wp_check_filetype_and_ext',
	function ( $wp_check_filetype_and_ext, $file, $filename, $mimes, $real_mime ) {

		if ( ! $wp_check_filetype_and_ext['type'] ) {

			$check_filetype  = wp_check_filetype( $filename, $mimes );
			$ext             = $check_filetype['ext'];
			$type            = $check_filetype['type'];
			$proper_filename = $filename;

			if ( $type && 0 === strpos( $type, 'image/' ) && 'svg' !== $ext ) {
				$ext  = false;
				$type = false;
			}

			$wp_check_filetype_and_ext = compact( 'ext', 'type', 'proper_filename' );
		}

		return $wp_check_filetype_and_ext;

	},
	10,
	5
);


    }

}


    add_settings_section(
        'e2es_plugin_section',
        'E2ES Settings',
        null,
        'e2es'
    );
    
}



