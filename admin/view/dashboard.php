<?php
$key = get_option('e2es_settings');
settings_fields('e2esPlugin');
do_settings_sections('e2esPlugin');
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.2/chart.min.js" integrity="sha512-zjlf0U0eJmSo1Le4/zcZI51ks5SjuQXkU0yOdsOBubjSmio9iCUp8XPLkEAADZNBdR9crRy3cniZ65LF2w8sRA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
<link rel="stylesheet" href="<?=plugin_dir_url( __FILE__ )?>../../../assets/css/dashboard.css" />

    <h2>Dashboard</h2>
<div class="modal fade widgetApp" style="display: none">
    <div class="modal-dialog" style="max-width: 80%;" >
        <div class="modal-content" id="widget-app">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <div class="flex-fill">
                        <h2 class="mb-0">Widgets</h2>
                    </div>
                </div>

                <div class="card-body p-4">
                    <div class="media-grid">
                        <?php 
                        global $wpdb;
                        
	$table_name = $wpdb->prefix . 'e2es_dashboardItems';


$result = $wpdb->get_results("SELECT * FROM $table_name");

                        foreach($result as $item) {
                        $item = json_decode(json_encode($item), FALSE); ?>
                        <div class="card">
                            <h4 class="card-header text-center text-truncate"><?=$item->name?></h4>
                            <div class="card-body text-center">
                                <p><?=$item->description?></p>
                            </div>
                            <div class="card-footer">
                                <button
                                        data-name="<?=$item->name?>"
                                        data-item="<?=$item->nameN?>"
										data-stat-array="<?=$item->statArray?>"
                                        data-type="<?=$item->type?>"
                                        data-min-width="<?=$item->minWidth?>" data-min-height="<?=$item->minHeight?>" class="btn btn-primary fg-add-item">
                                    <i class="fas fa-plus"></i>
                                    Add
                                </button>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="flexgrid-container" style="width: 100%;">
    <div class="flexgrid-helper">
        <button class="btn btn-sm edit-hidden fg-edit-widgets">Edit Mode</button>

        <button class="btn btn-sm edit-visible fg-add-widget">Add Widget</button>
        <button class="btn btn-sm edit-visible save-flexgrid fg-edit-widgets">Finish</button>
    </div>
    <div class="flexgrid-grid">
    </div>
    <div class="flex-grid-new">
        <div class="">
            <i class="fas fa-plus"></i>
        </div>
    </div>
</div>


    <?php
    $period = 'day';
    $date = 'lastMonth,today';
    $siteId = $key['e2es_siteId'];
    $authToken = $key['e2es_auth_token'];
    $query = "https://e68f-86-27-115-68.eu.ngrok.io/t.php?module=API&idSite=".$siteId."&period=".$period."&date=".$date."&format=JSON&token_auth=".$authToken;
    ?>

    <?php $data = wp_remote_get($query); ?>

    <script>
    let colours = ["#e60049", "#0bb4ff", "#50e991", "#e6d800", "#9b19f5", "#ffa300", "#dc0ab4", "#b3d4ff", "#00bfa0"];

        function pullData(report,label,type,statArray = null) {
            jQuery.ajax(
                {
                    type: "post",
                    crossDomain: true,
                    dataType: 'json',
                    url: "<?=$query?>&method="+report+(type !== 'lineChart' ? '&period=range' : ''),
                    success: function (msg) {
                        let fn = window[type];
                        if(typeof fn === "function") fn(report,label,msg,statArray);
                        //lineChart(report, label, msg)
                        //console.log(msg);
                    }
                });
        }

        function startLineChart(report,label) {
            jQuery.ajax(
                {
                    type: "post",
                    crossDomain: true,
                    dataType: 'json',
                    url: "<?=$query?>&method="+report,
                    success: function (msg) {
                        lineChart(report, label, msg)
                        //console.log(msg);
                    }
                });
        }
		
		function statTile(report,label,ajaxData,statArray) {
            const ctx = document.getElementById(report);
			ctx.parentElement.innerHTML = '<div class="stat-block"><div class="stat-value">'+ajaxData[statArray]+'</div><div class="stat-label">'+label+'</div></div>';
        }

        function lineChart(report,label,ajaxData,statArray = null) {
        const chartColours = colours.slice(0, 1);
            const ctx = document.getElementById(report).getContext('2d');
            const chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: Object.keys(ajaxData),
                    datasets: [{
                        label: label,
                        data: Object.values(ajaxData),
                        fill: false,
                    borderColor: chartColours,
                    backgroundColor: chartColours,
                    tension: 0.1,
                    pointRadius: 0
                    }],
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                }
            });
        }

        function pieChart(report,label,ajaxData,statArray = null) {
        ajaxData = ajaxData.slice(0,10);
            const ctx = document.getElementById(report).getContext('2d');
            const chart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: ajaxData.map(item => item.label),
                    datasets: [{
                        label: label,
                        data: ajaxData.map(item => item.nb_visits),
                    borderColor: colours,
                    backgroundColor: colours,
                    tension: 0.1
                    }],
                },
                options: {
                plugins: {
                    legend: {
                        position: 'right'
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
            }
            });
        }
    </script>

<script type="module">

    let res;
    $(function () {

        $.fn.setFlexGrid = function (options) {
            const defaults = { // default options for grid
                cols: 12, // starting amount of columns across a grid
                rows: 20, // starting amount of rows in a grid
                fixedGrid: false, // determines whether the grid can be resized or not (adding more rows)
                defaultHeight: 3, // amount of rows a widget will span upon creation
                defaultWidth: 3, // amount of columns a widget will span upon creation
                minHeight: 1, // minimum number of rows a widget can span ~ should be <= defaultHeight
                maxHeight: null, // if set to numeric value: maximum number of rows a widget can span
                minWidth: 1, // minimum number of columns a widget can span ~ should be <= defaultWidth
                maxWidth: null, // if set to numeric value: maximum number of columns a widget can span
                rowHeight: 1, // value times column width
                nested: false, // if true, widgets will allow nested widget to be dropped into them
                showGridlines: true, // if true, show gridlines on initialization
                animate: true, // determines wether or not the widgets should be animated
                nextAxis: 'x', // determines which axis the widget should find an open column. When X: widget will go to the first open column. When Y: widget will go to the first row that has an open column of x.
                resizeHandles: 'se', // determines resize handles (n, e, s, w, ne, se, sw, nw, all) ~ default is "se"
                checkRevert: true, // if widget cannot be dropped in pos (going to revert) because of minWidth / minHeight, placeholder will turn red
            };
            options = $.extend(defaults, options);

            const zoneInner = $(this);
            // var zoneInner = zone.find('.flexgrid-grid');

            // maintain a reference to the existing function
            const old_width = $.fn.width;
            // ...before overwriting the jQuery extension point
            $.fn.width = function (type, value) {
                // original behavior - use function.apply to preserve context
                const ret = old_width.apply(this, arguments);

                // stuff I will be extending that doesn't change the way .width() works

                if ($.type(type) == 'string' && type != undefined && type != null && type != '') {
                    const item = $(this);
                    switch (type) {
                        case 'minWidth':
                            if (value != undefined) { // set min-width
                                item.css('min-width', value);
                            } else { // get min-width
                                return parseInt(item.css('min-width'));
                            }
                            break;
                        case 'maxWidth':
                            if (value != undefined) { // set max-width
                                item.css('max-width', value);
                            } else { // get max-width
                                return parseInt(item.css('max-width'));
                            }
                            break;
                    }
                }

                // preserve return value (probably the jQuery object...)
                return ret;
            };

            // maintain a reference to the existing function
            const old_height = $.fn.height;
            // ...before overwriting the jQuery extension point
            $.fn.height = function (type, value) {
                // original behavior - use function.apply to preserve context
                const ret = old_height.apply(this, arguments);

                // stuff I will be extending that doesn't change the way .height() works

                if ($.type(type) == 'string' && type != undefined && type != null && type != '') {
                    const item = $(this);
                    switch (type) {
                        case 'minHeight':
                            if (value != undefined) { // set min-height
                                item.css('min-height', value);
                            } else { // get min-height
                                return parseInt(item.css('min-height'));
                            }
                            break;
                        case 'maxHeight':
                            if (value != undefined) { // set max-height
                                item.css('max-height', value);
                           } else { // get max-height
                                return parseInt(item.css('max-height'));
                            }
                            break;
                    }
                }

                // preserve return value (probably the jQuery object...)
                return ret;
            };

            // setup variables to be used in calculations in multiple functions
            $.fn.resetVars = function () {
                // reset variables since we perfected the zone-inner width / height
                const zoneInner = $(this); // we will need to reintialize this variable in every function where the following variables are used
                const zone = zoneInner.closest('.flexgrid-container');
                const zoneWidth = zoneInner.outerWidth();
                const zoneHeight = zoneInner.outerHeight();
                const colWidth = Math.floor(zoneWidth / options.cols); // width of each column
                const rowHeight = options.rowHeight > 1 ? options.rowHeight * colWidth : colWidth; // height of each row

                const res = {zone: zone, zW: zoneWidth, zH: zoneHeight, cW: colWidth, rH: rowHeight};
                return res;
            };
            var re = zoneInner.resetVars(); // reset zone variables

            // CREATE COLS
            $.fn.buildGrid = function () { // create rows of columns
                const zoneInner = $(this);
                const re = zoneInner.resetVars(); // reset zone variables
                const colAmount = options.cols; // number of columns in each row
                const rowAmount = options.rows; // number of rows in a zone
                const gridlines = options.showGridlines ? 'fg-gridlines' : '';

                for (let y = 0; y < rowAmount; y++) {
                    for (let x = 0; x < colAmount; x++) {
                        zoneInner.append('<div class="fg-enabled-col fg-col ' + gridlines + '" data-fg-eq="' + x + '" data-fg-row="' + y + '" style="min-width: ' + re.cW + 'px; min-height: ' + re.rH + 'px; top:' + (re.rH * y) + 'px; left: ' + (re.cW * x) + 'px; "></div>');
                        const appended = zoneInner.find('.fg-col[data-fg-row="' + y + '"][data-fg-eq="' + x + '"]');
                        const i = zoneInner.find('.fg-col').index(appended);
                        appended.attr('data-fg-index', i);
                    }
                }
                const lastCol = zoneInner.find('.fg-col').last();
                const rowCount = parseInt(lastCol.attr('data-fg-row'));
                zoneInner.css({ // reset the zone-inner width / height to perfect it.
                    'height': (rowCount + 1) * re.rH,
                    'width': (re.cW * options.cols),
                });
                re.zone.css({ // reset the zone width / height to perfect it.
                    'height': (rowCount + 1) * re.rH + 55, // add some pixels to allow space for the zone-helper
                    'width': (re.cW * options.cols) + 15, // add a little bezzel
                });
                enableSortable();
            };

            var re = zoneInner.resetVars(); // reset zone variables

            // set data attributes to find position of widget
            $.fn.setData = function () {

                const widget = $(this);
                const zoneInner = widget.closest('.flexgrid-grid');
                const zoneCol = widget.closest('.fg-col');
                const wigW = Math.floor(widget.width()); // widget width
                const wigH = Math.floor(widget.height()); // widget height

                const re = zoneInner.resetVars(); // reset zone variables
                const data_minWidth = parseFloat(Math.round(widget.width('minWidth') / re.cW));
                const data_minHeight = parseFloat(Math.round(widget.height('minHeight') / re.rH));
                const data_width = parseFloat(Math.round(wigW / re.cW)) < data_minWidth ? data_minWidth : parseFloat(Math.round(wigW / re.cW)); // number of columns a widget spans
                const data_height = parseFloat(Math.round(wigH / re.rH)) < data_minHeight ? data_minHeight : parseFloat(Math.round(wigH / re.rH)); // number of rows a widget spans
                const data_maxWidth = parseFloat(Math.round(widget.width('maxWidth') / re.cW));
                const data_maxHeight = parseFloat(Math.round(widget.height('maxHeight') / re.rH));
                const data_cardName = widget.find('.card').attr('data-cardName');
                const data_y = zoneCol.attr('data-fg-row'); // row the widget starts on ~ 0 indexed
                const data_x = zoneInner.find('.fg-col[data-fg-row="' + data_y + '"]').index(widget.closest('.fg-col[data-fg-row="' + data_y + '"]')); // column # the widget starts on ~ 0 indexed
                widget.attr({'data-app': widget.data('app'), 'data-cardName': data_cardName, 'data-fg-width': data_width, 'data-fg-height': data_height, 'data-fg-x': data_x, 'data-fg-y': data_y, 'data-fg-minwidth': data_minWidth, 'data-fg-minheight': data_minHeight, 'data-fg-maxwidth': data_maxWidth, 'data-fg-maxheight': data_maxHeight}); // set these new attributes


            };

            $.fn.setOption = function (option, val) {
                const widget = $(this);
                const grid = widget.closest('.flexgrid-grid');
                const re = grid.resetVars(); // reset zone variables

                let toggle;
                switch (option) {
                    case 'height':
                        toggle = 'data-fg-height';
                        widget.css('height', val * re.rH);
                        break;
                    case 'width':
                        toggle = 'data-fg-width';
                        widget.css('width', val * re.cW);
                        break;
                    case 'minHeight':
                        toggle = 'data-fg-minheight';
                        widget.css('min-height', val * re.rH);
                        break;
                    case 'minWidth':
                        toggle = 'data-fg-minwidth';
                        widget.css('min-width', val * re.cW);
                        break;
                    case 'maxHeight':
                        toggle = 'data-fg-maxheight';
                        widget.css('max-height', val * re.rH);
                        break;
                    case 'maxWidth':
                        toggle = 'data-fg-maxwidth';
                        widget.css('max-width', val * re.cW);
                        break;
                    case 'x':
                        toggle = 'data-fg-x';
                        const y = widget.attr('data-fg-y');
                        var col = grid.find('.fg-col[data-fg-eq="' + val + '"][data-fg-row="' + y + '"]');
                        widget.detach();
                        col.append(widget);
                        break;
                    case 'y':
                        toggle = 'data-fg-y';
                        const x = widget.attr('data-fg-x');
                        var col = grid.find('.fg-col[data-fg-eq="' + x + '"][data-fg-row="' + val + '"]');
                        widget.detach();
                        col.append(widget);
                        break;
                }
                widget.attr(toggle, val);
                widget.setData();
            };

            // enable or disable columns depending on the function parameter...
            $.fn.modCols = function (modifier) {
                const zoneCol = $(this);
                const zoneInner = zoneCol.closest('.flexgrid-grid');
                const widget = zoneCol.find('.fg-widget');
                if (modifier == 'disable') widget.setData();

                const rowStart = parseInt(zoneCol.attr('data-fg-row')); // row the widget starts on
                const rowEnd = rowStart + parseInt(widget.attr('data-fg-height')); // row the widget ends on

                const colStart = parseInt(zoneCol.attr('data-fg-eq')); // col the widget starts on
                const colEnd = colStart + parseInt(widget.attr('data-fg-width')); // col the widget ends on

                for (let r = rowStart; r < rowEnd; r++) {
                    for (let c = colStart; c < colEnd; c++) {
                        const self = zoneInner.find('.fg-col[data-fg-row="' + r + '"][data-fg-eq="' + c + '"]');
                        if (modifier == 'enable') { self.addClass('fg-enabled-col').removeClass('fg-disabled-col'); }
                        if (modifier == 'disable') { self.removeClass('fg-enabled-col').addClass('fg-disabled-col'); }
                    }
                }
            };

            $.fn.zoneOverflow = function () {
                let col = $(this);
                const widget = col.find('.fg-widget');
                const zoneInner = col.closest('.flexgrid-grid');
                const ogParent = widget.data('ogParent'); // original parent column
                const re = zoneInner.resetVars(); // reset zone variables
                let res = {obj: col};

                widget.setData();
                let data_x = parseInt(widget.attr('data-fg-x'));
                let data_y = parseInt(widget.attr('data-fg-y'));
                let data_width = parseInt(widget.attr('data-fg-width'));
                const data_cardName = widget.attr('data-cardName');
                let data_height = parseInt(widget.attr('data-fg-height'));
                let xCon = data_width + data_x > options.cols;
                let yCon = parseInt(col.attr('data-fg-row')) + data_height > parseInt(zoneInner.find('.fg-col').last().attr('data-fg-row')) + 1;
                const dif = ((data_width + data_x) - options.cols) * re.cW;

                if ($(ogParent).length > 0) { // if we dropped widget we should have an 'ogParent'
                    if ((options.cols - data_x) * re.cW < widget.width('minWidth')) { // if overflowing zone on x-axis
                        var de = col.find('.fg-widget').detach();
                        $(ogParent).append(de);
                        res = {obj: $(ogParent)};
                    } else if (xCon) { // if widget is overflowing zone on x-axis but is not at min-width
                        widget.css('width', widget.width() - dif);
                    }
                    res = {obj: col};
                } else if ($(ogParent).length <= 0) { // if we added new widget
                    let goHere = col;
                    while (xCon) { // detach the widget and append to the next open fg-enabled-col until it no longer overflows the zone width
                        var de = goHere.find('.fg-widget').detach();
                        goHere = goHere.nextAll('.fg-enabled-col').first();
                        if (goHere.length == 0 || goHere == undefined || goHere == null) {
                            zoneInner.createRow();
                            goHere = zoneInner.find('.fg-widget').last().closest('.fg-col').nextAll('.fg-enabled-col').first();
                        }
                        goHere.append(de);
                        de.setData(); // reset widget attributes

                        // reset position parameters to be used in next if statement / loop ...
                        data_x = parseInt(widget.attr('data-fg-x'));
                        data_y = parseInt(widget.attr('data-fg-y'));
                        data_width = parseInt(widget.attr('data-fg-width'));
                        data_height = parseInt(widget.attr('data-fg-height'));
                        yCon = yCon = parseInt(goHere.attr('data-fg-row')) + data_height > parseInt(zoneInner.find('.fg-col').last().attr('data-fg-row')) + 1;
                        xCon = data_width + data_x > options.cols;

                        if (!xCon) {
                            break;
                        }
                    }
                    col = goHere; // reset col so it can be used in the next if statment...
                    res = {obj: col};
                }
                if (yCon) { // if widget is overflowing zone height but is not at min-height
                    moreHeight(col); // add more rows...
                }

                data_x = parseInt(col.attr('data-fg-eq'));
                data_y = parseInt(col.attr('data-fg-row'));

                // tried to only target siblings that shared columns, but was much slower....
                let sibs = col.siblings('.fg-col');
                for (let a = 0; a < sibs.length; a++) {
                    const colSib = sibs.eq(a);
                    if (colSib.find('.fg-widget').length > 0) {
                        const dc = dropCollision(colSib, col);
                        col = dc.obj;
                        sibs = col.siblings('.fg-col');
                    }
                }
                res = {obj: col};

                return res;
            };

            // pass in parameters such as: x, y, width, height to position and size the widget
            $.fn.addWidget = function (params) {
                const zoneInner = $(this);
                const re = zoneInner.resetVars(); // reset zone variables
                let goHere;
                let width, height, minWidth, minHeight, maxWidth, maxHeight, nextAxis;

                // if no parameters are passed, use the default value. else check if the parameter has a value, if so use that value, else use default value
                const noParams = params == undefined || params == null ? true : false;
                width = (params.width == undefined ? options.defaultWidth : params.width) * re.cW;
                height = (params.height == undefined ? options.defaultHeight : params.height) * re.rH;

                minWidth = (params.minWidth == undefined ? options.minWidth : params.minWidth) * re.cW;
                minHeight = (params.minHeight == undefined ? options.minHeight : params.minHeight) * re.rH;

                maxWidth = (params.maxWidth == undefined ? options.maxWidth : params.maxWidth) * re.cW;
                maxWidth = (maxWidth == 0) ? 'unset' : maxWidth;
                maxHeight = (params.maxHeight == undefined ? options.maxHeight : params.maxHeight) * re.rH;
                maxHeight = (maxHeight == 0) ? 'unset' : maxHeight;

                nextAxis = params.nextAxis == undefined ? options.nextAxis : params.nextAxis;

                let amountNeeded = height / re.rH;
                if (zoneInner.find('.fg-enabled-col').length < 1) {
                    for (let l = 0; l < amountNeeded; l++) {
                        zoneInner.createRow();
                    }
                }

                // if we aren't given parameters or positions, then go to the first open column.
                if (params.x == undefined || params.y == undefined) {
                    var findNext = findNextColumn('x', zoneInner, params.x, params.y);
                    goHere = findNext.goHere;
                } else {
                    var findNext = findNextColumn(nextAxis, zoneInner, params.x, params.y);
                    goHere = findNext.goHere;
                }

                if (goHere.length == 0) console.error('Parent column does not exist.');

                const nested = options.nested == true ? $('<div class="fg-nested-container"></div>') : $('');
                const widget = noParams ? $('<div class="fg-widget"><i class="fa fa-times fg-remove-widget" title="remove this widget."></i><div class="fg-widget-inner fg-widget-handle"><div class="zone-txt text-center"></div>test</div></div>') : params.widget === undefined || params.widget === null ? $('<div class="fg-widget"><i class="fa fa-times remove-widget" title="remove this widget"></i><div class="fg-widget-inner fg-widget-handle"><div class="zone-txt text-center"></div>test32</div></div>') : params.widget;
                widget.find('.fg-widget-inner').append(nested);

                widget.css({ // set widget style options
                    'width': width,
                    'min-width': minWidth,
                    'max-width': maxWidth,
                    'height': height,
                    'min-height': minHeight,
                    'max-height': maxHeight,
                });

                goHere.append(widget); // append the widget
                widget.setData(); // reset the widget attributes

                // DETECT ZONE OVERFLOW
                const zo = goHere.zoneOverflow();
                goHere = zo.obj;
                let sibs = goHere.siblings('.fg-col');

                options.animate ? widget.animateWidget() : null;
                widget.setData();
                goHere.modCols('disable'); // disable overlapped columns

                resize(widget);
                enableSortable();
            };

            function findNextColumn(axis, zoneInner, x, y) {
                let goHere;
                switch (axis) {
                    case 'x':
                        x = x == undefined ? 0 : x;
                        zoneInner.find('.fg-col:nth-child(n+' + (x + 1) + ')').each(function () {
                            goHere = $(this);
                            if (!goHere.hasClass('fg-disabled-col') && goHere.find('.fg-widget').length == 0) {
                                goHere = $(this);
                                return false; // break when we find an open column
                            }
                        });
                        break;
                    case 'y':

                        if (x === undefined || x === null || x === '') { // if we still aren't passed an x
                            findNextColumn('x', zoneInner);
                        }
                        goHere = zoneInner.find('.fg-col[data-fg-row="' + y + '"][data-fg-eq="' + x + '"]');
                        while (goHere.hasClass('fg-disabled-col')) {
                            y++;
                            goHere = zoneInner.find('.fg-col[data-fg-row="' + y + '"][data-fg-eq="' + x + '"]');

                            if (!goHere.hasClass('fg-disabled-col')) {
                                break;
                            }
                        }
                        if (goHere.length == 0) {
                            const rowCount = zoneInner.find('.fg-col').last().attr('data-fg-row');
                            zoneInner.addRow(y - rowCount); // add as many rows as needed for placing the widget...
                            goHere = zoneInner.find('.fg-col[data-fg-row="' + y + '"][data-fg-eq="' + x + '"]');
                            goHere = goHere.hasClass('fg-disabled-col') ? goHere.nextAll('.fg-enabled-col[data-fg-row="' + (y) + '"][data-fg-eq="' + x + '"]').first() : goHere;
                        }
                        break;
                }

                const result = res = {goHere: goHere};
                return result;
            }

            $.fn.removeWidget = function (widget) {
                const zoneCol = widget.closest('.fg-col');
                zoneCol.modCols('enable');
                const originalContainer = widget.data('originalContainer') != undefined || widget.data('originalContainer') != null ? widget.data('originalContainer') : zoneCol;
                if (!originalContainer.hasClass('fg-col')) { // if the widget came from a different container
                    const de = widget.detach();
                    originalContainer.append(de);
                    de.css({'width': widget.data('ogWidth'), 'height': widget.data('ogHeight'), 'min-width': '', 'min-height': '', 'max-width': '', 'max-height': ''});
                } else {
                    widget.remove();
                }
                zoneCol.sortable('refresh');
            };

            $.fn.addRow = function (val) { // add specified number of rows outside of plugin...
                var zoneInner = $(this);
                val = val === undefined || val === null ? 1 : val;
                for (var i = 0; i < val; i++) {
                    zoneInner.createRow();
                }
            };

            $.fn.createRow = function () { // add a row to the grid ~ used internally
                const zoneInner = $(this);
                const re = zoneInner.resetVars(); // reset zone variables

                let lastCol = zoneInner.find('.fg-col').last(); // last column in zone
                const appendHere = lastCol[0].offsetTop + re.rH; // new row position
                let rowCount = parseInt(lastCol.attr('data-fg-row')) + 1; // number of rows found within zone
                const colAmount = options.cols; // number of columns spanning a row

                const gridlines = options.showGridlines ? 'fg-gridlines' : ''; // is show gridlines checked?
                for (let i = 0; i < colAmount; i++) {
                    zoneInner.append('<div class="fg-enabled-col fg-col ui-sortable ' + gridlines + '" data-fg-eq="' + i + '" data-fg-row="' + rowCount + '" style="min-width: ' + re.cW + 'px; min-height: ' + re.rH + 'px; top:' + appendHere + 'px; left: ' + (re.cW * i) + 'px; "></div>');
                    const blah = zoneInner.find('.fg-col[data-fg-row="' + rowCount + '"][data-fg-eq="' + i + '"]');
                    const n = zoneInner.find('.fg-col').index(blah);
                    blah.attr('data-fg-index', n);
                }
                lastCol = zoneInner.find('.fg-col').last(); // reset since we added rows
                rowCount = parseInt(lastCol.attr('data-fg-row')) + 1; // reset since we added rows
                zoneInner.height(rowCount * re.rH); // reset since we added rows
                re.zone.height(zoneInner.height() + 50); // reset since we added rows
                enableSortable();
            };

            function enableSortable() {
                $('.fg-col').sortable({
                    items: '.fg-widget',
                    connectWith: '.fg-enabled-col',
                    handle: '.fg-widget-handle',
                    cursor: 'move',
                    placeholder: 'fg-widget-placeholder',
                    tolerance: 'intersect',
                    start: function (event, ui) {
                        const zoneCol = $(this);
                        const zoneInner = zoneCol.closest('.flexgrid-grid');
                        const sibs = zoneCol.siblings('.fg-col');

                        const widget = zoneCol.find('.fg-widget');
                        zoneCol.modCols('enable'); // enable overlapped columns

                        const wig = $(this).find('.fg-widget-inner'); // set the placeholder's height and width equal to this widget.
                        const wigW = wig[0].offsetWidth;
                        const wigH = wig[0].offsetHeight;
                        zoneInner.find('.fg-widget-placeholder').css({
                            'width': wigW,
                            'height': wigH,
                        });

                        // set data so we can see if it changes on stop
                        ui.item.data({
                            'ogIndex': ui.item.closest('.fg-col').index($(this)), // original index value of the widget
                            'ogParent': ui.item.closest('.fg-col'), // original parent column of the widget
                        });
                    },
                    over: function (event, ui) {
                        // refresh sortable columns only when we are dragging over them
                        // previously called in sortable start and that caused a lot of lag with multiple widgets...
                        const zoneCol = $(this);
                        zoneCol.sortable('refresh');

                        const sibs = zoneCol.siblings('.fg-col');
                        const widget = ui.item;

                        // this is for widgets coming from outside of a grid...
                        if (!widget.data('sortableItem').bindings.hasClass('.fg-col')) {
                            const originalContainer = widget.data('sortableItem').bindings;
                            const ogWidth = widget.data('sortableItem').helperProportions.width;
                            const ogHeight = widget.data('sortableItem').helperProportions.height;
                            $(this).find('.ui-sortable-placeholder').addClass('fg-widget-placeholder');
                            // var minWidth = ui.item.attr('data-fg-minwidth') != options.minWidth ? ui.item.attr('data-fg-minwidth') : options.minWidth * re.cW;
                            // var minHeight = ui.item.attr('data-fg-minheight') != options.minHeight ? ui.item.attr('data-fg-minheight') : options.minHeight * re.rH;
                            // $(this).find('.ui-sortable-placeholder').css({ 'width': (Math.ceil((ogWidth / re.cW)) * re.cW) - 10, 'height': (Math.ceil((ogHeight / re.rH)) * re.rH) - 10, 'min-width': minWidth - 10, 'min-height':minHeight - 10, 'max-width': options.maxWidth != null && options.maxWidth != undefined ? options.maxWidth * re.cW - 10 : 'unset', 'max-height': options.maxWidth != null && options.maxWidth != undefined ? options.maxWidth * re.cW - 10 : 'unset', 'visibility': 'visible', 'position':'absolute' });
                            // ui.item.css({ 'width': Math.ceil((ogWidth / re.cW)) * re.cW, 'height': Math.ceil((ogHeight / re.rH)) * re.rH, 'min-width': minWidth, 'min-height': minHeight, 'max-width': options.maxWidth != null && options.maxWidth != undefined ? options.maxWidth * re.cW : 'unset', 'max-height': options.maxWidth != null && options.maxWidth != undefined ? options.maxWidth * re.cW : 'unset' });
                        }

                        if (options.checkRevert) {
                            // check if widget can be dropped here or if it will revert to it's original position
                            widget.checkCollision('sort', sibs); // check for collision
                        }
                    },
                    receive: function (event, ui) {
                        let zoneCol = $(this);
                        const zoneInner = zoneCol.closest('.flexgrid-grid');
                        const re = zoneInner.resetVars();
                        let sibs = zoneCol.siblings('.fg-col');
                        const zone = zoneCol.closest('.flexgrid-container');

                        ui.item.setData(); // reset here so that zoneOverflow can use the attributes

                        if (!ui.item.data('sortableItem').bindings.hasClass('fg-col')) {
                            const originalContainer = ui.item.data('sortableItem').bindings;
                            const ogWidth = ui.item.data('sortableItem').helperProportions.width;
                            const ogHeight = ui.item.data('sortableItem').helperProportions.height;
                            ui.item.data({
                                'originalContainer': originalContainer,
                                'ogWidth': ogWidth,
                                'ogHeight': ogHeight,
                            }); // set the original container data
                            ui.item.css({
                                'width': Math.ceil((ogWidth / re.cW)) * re.cW,
                                'height': Math.ceil((ogHeight / re.rH)) * re.rH,
                                'minWidth': options.minWidth * re.cW,
                                'minHeight': options.minHeight * re.rH,
                                'maxWidth': options.maxWidth != null && options.maxWidth != undefined ? options.maxWidth * re.cW : 'unset',
                                'maxHeight': options.maxWidth != null && options.maxWidth != undefined ? options.maxWidth * re.cW : 'unset',
                            });
                        }

                        const zo = zoneCol.zoneOverflow(); // check if the widget is overflowing the zone
                        zoneCol = zo.obj;
                        sibs = zoneCol.siblings('.fg-col');

                        zoneCol.modCols('disable');

                        ui.item.setData(); // reset widget attributes
                        options.animate ? ui.item.animateWidget() : null;
                        resize(ui.item);
                    },
                    stop: function (event, ui) {
                        let zoneCol = $(this);
                        const widget = zoneCol.find('.fg-widget');
                        let sibs = zoneCol.siblings('.fg-col');
                        const ogParent = ui.item.data('ogParent');

                        const zo = zoneCol.zoneOverflow(); // check if the widget is overflowing the zone
                        zoneCol = zo.obj;
                        sibs = zoneCol.siblings('.fg-col');

                        zoneCol.modCols('disable'); // disable overlapped columns

                        // ui.item.setData(); // set data attributes
                        options.animate ? ui.item.animateWidget() : null;
                        resize(ui.item);

                        // detect if the item position has changed so that we can remind the user to save...
                        if (ui.item.closest('.fg-col').index(zoneCol) != ui.item.data('ogIndex')) {
                        }
                    },
                });
            }

            enableSortable();

            var re = zoneInner.resetVars(); // reset zone variables
            // Resize function
            function resize(widget) {
                widget.resizable({
                    grid: [re.cW, (options.rowHeight > 1 ? options.rowHeight * re.cW : re.cW)],
                    handles: options.resizeHandles,
                    containment: zoneInner,
                    start: function (event, ui) {
                        const widget = ui.element;
                        const zoneCol = widget.closest('.fg-col');
                        const sibs = zoneCol.siblings('.fg-col');
                        zoneCol.modCols('enable'); // enable overlapped columns

                        for (let i = 0; i < sibs.length; i++) {
                            const colSib = sibs.eq(i);
                            if (colSib.children('.fg-widget').length > 0 && (colSib.offset().left == zoneCol.offset().left + zoneCol.width() || colSib.offset().top == zoneCol.offset().top + zoneCol.height())) {
                                zoneCol.checkCollision('resize', colSib); // check for collision when resizing
                            }
                        }

                        ui.element.data({
                            'ogSize': {width: ui.element[0].offsetWidth, height: ui.element[0].offsetHeight}, // find the original size of item so we can later detect if it has changed.
                            'ogParent': ui.element.closest('.fg-col'), // original parent column of the widget
                        });
                    },
                    resize: function (event, ui) {
                        // DETECT COLLISION
                        const widget = ui.element;
                        const zoneCol = widget.parent();
                        const sibs = zoneCol.siblings('.fg-col');
                        // so that we can continue to resize once there is no collision
                        ui.element.resizable('option', 'maxHeight', null);
                        ui.element.resizable('option', 'maxWidth', null);

                        for (let i = 0; i < sibs.length; i++) {
                            const colSib = sibs.eq(i);
                            if (colSib.children('.fg-widget').length > 0 && (colSib.offset().left == zoneCol.offset().left + zoneCol.width() || colSib.offset().top == zoneCol.offset().top + zoneCol.height())) {
                                zoneCol.checkCollision('resize', colSib); // check for collision when resizing
                            }
                        }
                        widget.setData(); // reset widget attributes
                    },
                    stop: function (event, ui) {
                        const widget = ui.element;
                        const zoneCol = widget.closest('.fg-col');
                        const sibs = zoneCol.siblings('.fg-col');

                        for (let i = 0; i < sibs.length; i++) {
                            const colSib = sibs.eq(i);
                            if (colSib.find('.fg-widget').length > 0) {
                                zoneCol.checkCollision('resize', colSib); // check for collision when resizing
                                dropCollision(colSib, zoneCol); // check for collision on stop, just in case...
                            }
                        }
                        widget.setData(); // reset widget attributes
                        zoneCol.modCols('disable'); // disable overlapped columns
                        options.animate ? widget.animateWidget() : null;

                        // reset max height and width
                        widget.resizable('option', 'maxHeight', null);
                        widget.resizable('option', 'maxWidth', null);

                    },
                });
            }

            $.fn.checkCollision = function (type, colSib) {
                const el = $(this);

                const x1 = el[0].offsetLeft; // widget left position
                const y1 = el[0].offsetTop; // widget top position
                const b1 = y1 + el[0].offsetHeight; // widget bottom position
                const r1 = x1 + el[0].offsetWidth; // widget right position

                const x2 = colSib[0].offsetLeft; // collided widget left position
                const y2 = colSib[0].offsetTop; // collided widget top position
                const b2 = y2 + colSib[0].offsetHeight; // collided widget bottom position
                const r2 = x2 + colSib[0].offsetWidth; // collided widget right position

                // detect when a widget has collided with another during resize, then prevent resizing through that widget...
                if (type == 'resize') {
                    // X-AXIS
                    if ((r1 == x2 && y1 == y2 || r1 == x2 && b1 == b2)
                        || (y2 < y1 && b2 > b1 && r2 > r1)
                        || (y1 < y2 && b1 > b2 && r2 > r1)
                        || (y2 < b1 && b2 > b1 && r1 == x2)
                        || (y1 < b2 && b1 >= b2 && y1 >= y2 && r1 >= x2 && x1 < x2)) {
                        $('.ui-resizable-resizing').resizable('option', 'maxWidth', (x2 - x1));
                    }
                    // Y-AXIS
                    if ((b1 == y2 && x1 == x2 || b1 == y2 && r1 == r2)
                        || (x1 < x2 && r1 > r2 && b1 == y2)
                        || (x2 < x1 && r2 > r1 && b1 == y2)
                        || (r2 > r1 && x2 < r1 && b1 == y2)
                        || (r1 > r2 && x1 < r2 && b1 == y2)) {
                        $('.ui-resizable-resizing').resizable('option', 'maxHeight', (y2 - y1));
                    }
                } else if (type == 'sort') {
                    const widget = el;
                    const placeholder = widget.data().sortableItem.placeholder;
                    placeholder.removeClass('danger-placeholder');
                    const zoneCol = placeholder.parent();
                    const zoneInner = zoneCol.closest('.flexgrid-grid');
                    const re = zoneInner.resetVars(); // reset zone variables

                    const width = parseInt(widget.attr('data-fg-width'));
                    const height = parseInt(widget.attr('data-fg-height'));

                    const minWidth = parseInt(widget.attr('data-fg-minwidth'));
                    const minHeight = parseInt(widget.attr('data-fg-minheight'));

                    const rowStart = parseInt(zoneCol.attr('data-fg-row'));
                    const rowEnd = rowStart + minHeight - 1;

                    const colStart = parseInt(zoneCol.attr('data-fg-eq'));
                    const colEnd = colStart + minWidth - 1;

                    const danger = [];

                    for (let i = 0; i < colSib.length; i++) {
                        const item = colSib.eq(i);
                        if (item.hasClass('fg-disabled-col')
                            && (item.attr('data-fg-eq') >= colStart && item.attr('data-fg-eq') <= colEnd)
                            && (item.attr('data-fg-row') >= rowStart && item.attr('data-fg-row') <= rowEnd)
                        ) {
                            danger.push('');
                        }
                    }

                    if ((options.cols - colStart) * re.cW < widget.width('minWidth')) {
                        danger.push('');
                    }

                    if (danger.length) {
                        placeholder.addClass('danger-placeholder');
                    }

                }

            };

            // check for collision when dropping a widget, if it overlaps another widget.
            function dropCollision(colSib, col) {
                const widget = col.find('.fg-widget');
                const zoneInner = col.closest('.flexgrid-grid');
                const re = zoneInner.resetVars(); // reset zone variables

                const ogParent = widget.data('ogParent'); // original parent column
                res = {obj: col};

                // column postions
                let x1 = col.offset().left;
                let y1 = col.offset().top;
                let b1 = y1 + col.height();
                let r1 = x1 + col.width();

                // current sibling column positions
                const x2 = colSib.offset().left;
                const y2 = colSib.offset().top;
                const b2 = y2 + colSib.height();
                const r2 = x2 + colSib.width();

                // if the column is colliding with the sibling column at any position
                let con = (r1 > x2 && x1 < x2 && y1 < y2 && b1 > y2) || (r1 > x2 && x1 < x2 && y1 == y2 && b1 == b2) || (r1 > x2 && x1 < x2 && y1 <= y2 && b1 >= b2) || (r1 > x2 && x1 < x2 && y1 >= y2 && b1 <= b2) || (r1 > x2 && x1 < x2 && y1 < b2 && b1 > b2) || (x1 < r2 && r1 > r2 && y1 < y2 && b1 > y2) || (x1 >= x2 && r1 <= r2 && y1 < y2 && b1 > y2);
                // if the columns x-axis is colliding with the sibling columns x-axis
                const xCon = (y1 <= y2 && x1 < x2 && r1 > x2 && b1 > y2) || (y1 < y2 && b1 > b2 && x1 < x2 && r1 > x2) || (b1 >= b2 && y1 < b2 && x1 < x2 && r1 > x2) || (y1 > y2 && b1 < b2 && x1 < x2 && r1 > x2) || (y1 == y1 && b1 == b2 && x1 < x2 && r1 > x2);
                // if the columns y-axis is colliding with the sibling columns y-axis
                const yCon = (x1 < r2 && r1 > r2 && y1 < y2 && b1 > y2) || (x1 >= x2 && r1 <= r2 && y1 < y2 && b1 > y2) || (x1 <= x2 && r1 >= r2 && y1 < y2 && b1 > y2) || (r1 > x2 && x1 < x2 && y1 < y2 && b1 > y2);

                if ($(ogParent).length > 0) { // if we dragged from another column
                    if (xCon && yCon) { // if both the x-axis and y-axis have collision
                        const xConVal = x2 - x1;
                        const yConVal = y2 - y1;
                        if (xConVal > yConVal) { // find which axis has more collision and resize for that one
                            xCollision();
                        } else {
                            yCollision();
                        }
                    } else if (xCon) { // if only x-axis collision, resize for x-axis
                        xCollision();
                    } else if (yCon) { // if only y-axis collision, resize for y-axis
                        yCollision();
                    }

                    function xCollision() { // x-axis collision
                        const saveWidth = widget.width(); // save the width
                        widget.width(x2 - x1);
                        if (x2 - x1 < widget.width('minWidth')) { // if new width is less than the minWidth ~
                            const de = widget.detach();
                            de.width(saveWidth); // ~ resize back to saveWidth
                            $(ogParent).append(de); // ~ and revert to ogParent
                        }
                    }

                    function yCollision() { // y-axis collision
                        const saveHeight = widget.height(); // save the height
                        widget.height(y2 - y1);
                        if (y2 - y1 < widget.height('minHeight')) { // if new height is less than the minHeight ~
                            const de = widget.detach();
                            de.height(saveHeight); // ~ resize back to saveHeight
                            $(ogParent).append(de); // ~ and revert to ogParent
                        }
                    }
                } else if ($(ogParent).length <= 0) { // if we added a new widget
                    let goHere = col;
                    while (con) { // detach the widget and append to the next open fg-enabled-col until there is no collision
                        var de = goHere.find('.fg-widget').detach();
                        goHere = goHere.nextAll('.fg-enabled-col').first();
                        if (goHere.length == 0 || goHere == undefined || goHere == null) {
                            zoneInner.createRow();
                            goHere = zoneInner.find('.fg-widget').last().closest('.fg-col').nextAll('.fg-enabled-col').first();
                        }
                        goHere.append(de);
                        de.setData();

                        // check for zone overflow, if true head the widget will head for the next column and check for collision again
                        const zo = goHere.zoneOverflow();
                        goHere = zo.obj;

                        // reset position parameters
                        x1 = goHere.offset().left;
                        y1 = goHere.offset().top;
                        b1 = y1 + goHere.height();
                        r1 = x1 + goHere.width();

                        // reset condition, otherwise the position variables will still be attached to "el" rather than "goHere"
                        con = (r1 > x2 && x1 < x2 && y1 < y2 && b1 > y2) || (r1 > x2 && x1 < x2 && y1 == y2 && b1 == b2) || (r1 > x2 && x1 < x2 && y1 <= y2 && b1 >= b2) || (r1 > x2 && x1 < x2 && y1 >= y2 && b1 <= b2) || (r1 > x2 && x1 < x2 && y1 < b2 && b1 > b2) || (x1 < r2 && r1 > r2 && y1 < y2 && b1 > y2) || (x1 >= x2 && r1 <= r2 && y1 < y2 && b1 > y2);

                        if (!con) {
                            break;
                        }
                    }
                    res = {obj: goHere};
                }
                return res;
            }

            function moreHeight(el) { // when a widget is appended but it overflows the zone height, append more rows until the height + the data-fg-row == the last rows 'data-fg-row'
                const zoneInner = el.closest('.flexgrid-grid');
                const widget = el.find('.fg-widget');
                const row_and_height = parseInt(el.attr('data-fg-row')) + parseInt(widget.attr('data-fg-height'));
                const last_row_num = parseInt(zoneInner.find('.fg-col').last().attr('data-fg-row'));
                if (row_and_height != last_row_num) {
                    const amountNeeded = (row_and_height - last_row_num) - 1;
                    for (let n = 0; n < amountNeeded; n++) {
                        zoneInner.createRow();
                    }
                }
                widget.setData();
                widget.modCols('disable');
            }

            $.fn.animateWidget = function () {
                const widget = $(this).find('.fg-widget-inner');
                widget.queue('fx', function (next) {
                    $(this).addClass('animate');
                    next();
                });
                widget.delay(400).queue('fx', function (next) {
                    $(this).removeClass('animate');
                    next();
                });
            };

            $.fn.saveGrid = function () {
                const zoneInner = $(this);
                const array = [
                    {
                        cols: options.cols,
                        rows: options.rows,
                        widgets: [],
                    },
                ];
                const widgets = zoneInner.find('.fg-widget');
                $(widgets).each(function () {
                    const widget = $(this);
                    array[0]['widgets'].push({
                        x: widget.attr('data-fg-x'),
                        y: widget.attr('data-fg-y'),
                        width: widget.attr('data-fg-width'),
                        height: widget.attr('data-fg-height'),
                        app: widget.attr('data-app'),
                    });
                });
                return array;
            };
        };

        var zone = $('.flexgrid-container');
        var zoneInner = zone.find('.flexgrid-grid');
        zoneInner.setFlexGrid({
            cols: 12,
            rows: 6,
        });

        zoneInner.buildGrid();

        $(document).on('click', '.fg-edit-widgets', function () {
            $(this).closest('.flexgrid-container').toggleClass('editable');
        });

        $(document).on('click', '.flex-grid-new', function () {
            zoneInner.addRow();
        });
<?php
$prefix = $wpdb->prefix;

$result = $wpdb->get_results("SELECT di.*, dai.width, dai.height, dai.x, dai.y FROM {$prefix}e2es_account_dashboardItems dai
INNER JOIN {$prefix}e2es_dashboardItems di on di.id = dai.dashboardItemId");
                        ?>

        let $data = <?=json_encode($result);?>;

        $.each($data, function (key, value) {
            var widget = $(`
                  <div class="fg-widget" data-app="${value.nameN}">
                    <i class="fa fa-times fg-remove-widget" title="Remove this widget"></i>
                    <i class="fas fa-arrows-alt move-widget fg-widget-handle"></i>
                    <div class="fg-widget-inner">
                      <div class="widget-app-container"><canvas id="${value.nameN}" ></canvas></div>
                    </div>
                  </div>`);
            zoneInner.addWidget({
                widget: widget,
                x: value.x, y: value.y,
                width: value.width, height: value.height,
                minWidth: value.minWidth, minHeight: value.minHeight,
            });
            pullData(value.nameN,value.name,value.type,value.statArray);
        });



        $(document).on('click', '.fg-add-item', function () {
            const app = $(this).attr('data-item');
            const width = $(this).attr('data-min-width');
            const height = $(this).attr('data-min-height');
            const name = $(this).attr('data-name');
            const type = $(this).attr('data-type');
            const statArray = $(this).attr('data-stat-array');
            zoneInner.addWidget({
                widget: $(`
                  <div class="fg-widget" data-app="${app}">
                    <i class="fa fa-times fg-remove-widget" title="Remove this widget"></i>
                    <i class="fas fa-arrows-alt move-widget fg-widget-handle"></i>
                    <div class="fg-widget-inner">
                      <div class="widget-app-container"><canvas id="${app}" ></canvas></div>
                    </div>
                  </div>`), width: width, minWidth: width, height: height, minHeight: height,
            });
            $('.widgetApp').modal('toggle');
            pullData(app,name,type,statArray);
        });


        $(document).on('click', '.fg-add-widget', function () {
            $('.widgetApp').modal('toggle');
        });

        $(document).on('click', '.fg-remove-widget', function () {
            const widget = $(this).closest('.fg-widget');
            zoneInner.removeWidget(widget);
        });
        $(document).on('click', '.save-flexgrid', function () {
            const grid = zoneInner.saveGrid();
            console.log(grid[0].widgets);
        });


        $('.widget-holder').sortable({
            connectWith: '.fg-enabled-col', // connect to the grid columns
            items: '.fg-widget', // make sure the only sortable items are the widgets
            handle: '.fg-widget-handle', // include sortable handle
            helper: 'clone',
            appendTo: 'body',
        });

    });
</script>
