<?php

	$tabList =[
		[
            'tabName' => 'general',
            'displayName' => 'General Settings'
        ],
		[
            'tabName' => 'footfall',
            'displayName' => 'Footfall'
        ],
        [
            'tabName' => 'overrides',
            'displayName' => 'Overrides'
        ]
	];
$options = get_option('e2es_settings');
?>
<div class="wrap">
    <h1></h1>
	<div class="e2es-title-bar">
		<h1>E2E Studios</h1>
		<p>
			Creators of random stuff
		</p>
	</div>
    
    <div class="e2es-content-wrapper">
        <div class="e2es-content-main">
		<?php settings_errors(); ?>
		<?php $active_tab = array_search($_GET['tab'], array_column($tabList, 'tabName')) !== false ? $_GET['tab'] : 'general';
?>
		<h2 class="nav-tab-wrapper">
            <?php foreach($tabList as $tl) { ?>
                <a href="?page=e2es&tab=<?=$tl['tabName']?>" class="nav-tab <?=$active_tab == $tl['tabName'] ? 'nav-tab-active' : ''?>"><?=$tl['displayName']?></a>
            <?php } ?>
	</h2>

			<div class="e2es-tab-content">
				
<?php
   include(plugin_dir_path(__FILE__) . sprintf('%s.php',$active_tab));

?>
			</div>
    </div>
</div>

