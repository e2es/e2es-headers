<form action='options.php' method='post'>
    <h2>Footfall Settings</h2>

        <?php
        $key = get_option('e2es_settings');
        $key = $key == '' ? [] : $key;
        settings_fields('e2esPlugin');
        do_settings_sections('e2esPlugin');
        ?>

            <table class="form-table" role="presentation">
        <tbody>
        
        <tr>
            <th scope="row">Enable Footfall Tracking</th>
            <td>

                <label class="switch">
                    <input type="checkbox" name="e2es_settings[e2es_has_tracking]" class="switch-input" <?= $key['e2es_has_tracking'] ? 'checked' : ''?>>
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </td>
        </tr>

        <tr>
            <th scope="row">Allow SVG Uploads</th>
            <td>

                <label class="switch">
                    <input type="checkbox" name="e2es_settings[e2es_has_svg_upload]" class="switch-input" <?= $key['e2es_has_svg_upload'] ? 'checked' : ''?>>
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </td>
        </tr>

        <tr>
            <th scope="row">Enable ACF Options Page</th>
            <td>

                <label class="switch">
                    <input type="checkbox" name="e2es_settings[e2es_has_acf_options]" class="switch-input" <?= $key['e2es_has_acf_options'] ? 'checked' : ''?>>
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </td>
        </tr>


        </tbody>
    </table>

    <?php submit_button(); ?>
</form>
