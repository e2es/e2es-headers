<form action='options.php' method='post'>
    <?php
    $header = get_option('e2es_header');
    $footer = get_option('e2es_footer');
    $body = get_option('e2es_body');
    settings_fields('e2esPluginOverrides');
    do_settings_sections('e2esPluginOverrides');
    ?>
    <h2>Override Settings</h2>

    <div class="wrap">
            <p>
                <label for="e2es_header"><strong>Scripts In Header</strong>
                </label>
                <textarea name="e2es_header" id="e2es-header" class="widefat nice-editor" rows="8"  <?=(! current_user_can( 'unfiltered_html' ) ) ? ' disabled="disabled" ' : '' ?>><?= $header ?></textarea>
                These scripts will be printed in the <code>&lt;head&gt;</code> section.
            </p>
        <p>
            <label for="e2es_body"><strong>Scripts In Body</strong>
            </label>
            <textarea name="e2es_body" id="e2es-body" class="widefat nice-editor" rows="8"  <?=(! current_user_can( 'unfiltered_html' ) ) ? ' disabled="disabled" ' : '' ?>><?= $body ?></textarea>
            These scripts will be printed just below the opening <code>&lt;body&gt;</code> tag.

        </p>
        <p>
            <label for="e2es_footer"><strong>Scripts In Footer</strong>
            </label>
            <textarea name="e2es_footer" id="e2es-footer" class="widefat nice-editor" rows="8"  <?=(! current_user_can( 'unfiltered_html' ) ) ? ' disabled="disabled" ' : '' ?>><?= $footer ?></textarea>
            These scripts will be printed above the closing <code>&lt;body&gt;</code> tag.
        </p>
    </div>

    <?php submit_button(); ?>
    <script>
        jQuery(document).ready(function($) {
            wp.codeEditor.initialize($('#e2es-header'), cm_settings);
            wp.codeEditor.initialize($('#e2es-body'), cm_settings);
            wp.codeEditor.initialize($('#e2es-footer'), cm_settings);
        })
    </script>
</form>
