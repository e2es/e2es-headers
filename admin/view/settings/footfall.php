<form action='options.php' method='post'>
    <h2>Footfall Settings</h2>

        <?php
        $key = get_option('e2es_footfall_settings');
        $key = $key == '' ? [] : $key;
        settings_fields('e2esPluginFootfall');
        do_settings_sections('e2esPluginFootfall');
        ?>

            <table class="form-table" role="presentation">
                <tbody>

                <tr>
                    <th scope="row">Auth Token (For dashboard only)</th>
                    <td><input type="text" value="<?= $key['e2es_auth_token'] ?>" name="e2es_footfall_settings[e2es_auth_token]"/></td>
                </tr>
                <tr>
                    <th scope="row">Site ID</th>
                    <td>
                        <input type="text" value="<?= $key['e2es_siteId'] ?>" name="e2es_footfall_settings[e2es_siteId]"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Track Visitors on all subdomains</th>
                    <td>
                        <input type="checkbox" value="true" name="e2es_footfall_settings[e2es_subdomains]" <?=$key['e2es_subdomains'] ? 'checked' : ''?>>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Prepend the site domain to the page title</th>
                    <td>
                        <input type="checkbox" value="true" name="e2es_footfall_settings[e2es_prepend]" <?=$key['e2es_prepend'] ? 'checked' : ''?>>
                    </td>
                </tr>
                <tr>
                    <th scope="row">In the 'Outlinks' report, hide clicks to know alias URLs</th>
                    <td>
                        <input type="checkbox" value="true" name="e2es_footfall_settings[e2es_hideAlias]" <?=$key['e2es_hideAlias'] ? 'checked' : ''?>>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Track users with JavaScript disabled</th>
                    <td>
                        <input type="checkbox" value="true" name="e2es_footfall_settings[e2es_javaScript]" <?=$key['e2es_javaScript'] ? 'checked' : ''?>>
                    </td>
                </tr>
                </tbody>
            </table>

    <?php submit_button(); ?>
</form>
