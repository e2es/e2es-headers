    === E2E Studios ===
    Contributors: Michael Pardon & Matthew McMahon
    Donate link: https://e2estudios.com
    Tags: headers, footers, matomo
    Requires at least: 4.7
    Tested up to: 5.8
    Stable tag: 4.3
    Requires PHP: 7.4
    License: GPLv2 or later
    License URI: https://www.gnu.org/licenses/gpl-2.0.html

This is an E2E Studios custom plugin to enable features such as header & footer integrations, footfall analytics and other helpful additions.

    == Description ==

    This is the E2E Studios custom plugin

    == Changelog ==

    = 1.0 =
    * Creating Repo
