<?php

$header = get_option('e2es_header');
$footer = get_option('e2es_footer');
$body = get_option('e2es_body');

// Frontend Hooks
add_action( 'wp_head', 'e2esheader');
add_action( 'wp_footer', 'e2esfooter');
if (function_exists( 'wp_body_open' ) && version_compare( get_bloginfo( 'version' ), '5.2',)) {
    add_action( 'wp_body_open', 'e2esBody' ,1 );
}



function insertFootfall() {
    $key = get_option('e2es_settings');
    $key2 = get_option('e2es_footfall_settings');
    if($key !== '' && $key2 !== '') {
    if($key2['e2es_siteId'] && $key['e2es_has_tracking']) {
        $site = '//portal.footfall.pro/';
        $prepend = $key2['e2es_prepend'] ? '
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);' : '';
        $cookieDomain = $key2['e2es_subdomains'] ? '_paq.push(["setCookieDomain", "*.'.$_SERVER['SERVER_NAME'].'"]);' : '';
        $alias = $key2['e2es_hideAlias'] ? '_paq.push(["setDomains", ["*.'.$_SERVER['SERVER_NAME'].'"]]);' : '';
        $noJavaScript = $key2['e2es_javaScript'] ? '<noscript><p><img src="'.$site.'matomo.php?idsite='.$key2['e2es_siteId'].'&amp;rec=1" style="border:0;" alt="" /></p></noscript>' : '';
        return <<<HTML
    <!-- Footfall Analytics -->
<script>
  var _paq = window._paq = window._paq || [];
  {$cookieDomain}
  {$prepend}
  {$alias}
    _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u= "{$site}";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '{$key2['e2es_siteId']}']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
{$noJavaScript}

HTML;

    }
    }
}


function e2esheader() {
    echo "<!-- E2E Studios Extensions Snippet -->";
    echo get_option('e2es_header');
    echo insertFootfall();
    echo "<!-- End of E2E Studios Extensions Snippet -->";
}


function e2esfooter() {
    echo "<!-- E2E Studios Extensions Snippet -->";
    echo get_option('e2es_footer');
    echo "<!-- End of E2E Studios Extensions Snippet -->";
}


function e2esbody() {
    echo "<!-- E2E Studios Extensions Snippet -->";
    echo get_option('e2es_body');
    echo "<!-- End of E2E Studios Extensions Snippet -->";
}
