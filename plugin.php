<?php
/*
Plugin Name:	E2E Studios Extensions
Plugin URI:		https://e2estudios.com
Description:	This is an E2E Studios custom plugin to enable features such as header & footer integrations, footfall analytics and other helpful additions.
Version:		1.0.21
Author:			E2E Studios
Author URI:		https://e2estudios.com
License:		GPL-2.0+
License URI:	http://www.gnu.org/licenses/gpl-2.0.txt

This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This plugin. If not, see {URI to Plugin License}.
*/

if (!defined('WPINC')) {
    die;
}

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/e2es/e2es-headers',
    __FILE__,
    'e2es-headers'
);



register_activation_hook( __FILE__, 'e2es_db' );
function e2es_db() {
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'e2es_dashboardItems';

if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
    {
	$sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(768) NOT NULL,
        nameN varchar(768) NOT NULL,
        description TEXT,
        minWidth INT(11) DEFAULT 1 NOT NULL,
        minHeight INT(11) DEFAULT 1 NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
    }

    $table_name = $wpdb->prefix . 'e2es_account_dashboardItems';

if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
    {
	$sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
        dashboardItemId INT(11) NOT NULL,
        x INT(11) NOT NULL,
        y INT(11) NOT NULL,
        width INT(11) NOT NULL,
        height INT(11) NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
    }
}


//Set the branch that contains the stable release.
//$myUpdateChecker->setBranch('master');

if(is_admin()) {
    include(plugin_dir_path(__FILE__) . 'admin/main.php');
} else {
    include(plugin_dir_path(__FILE__) . 'main.php');
}

add_action('wp_dashboard_setup', 'addDashboardWidget');
function addDashboardWidget() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Website Support', 'custom_dashboard_help');
}
function custom_dashboard_help() {
    echo '<p>Welcome to your E2E Studios Website. If you need any help with this portal or have any queries regarding your website please get in touch at <a href="https://e2estudios.com/contact" target="_blank">e2estudios.com/contact</a>.</p>';
}

/* Add message above login form */
function login_message() {
         wp_enqueue_style('custom-css', plugins_url('/assets/css/custom.css', __FILE__));
	return '<p class="message">Welcome to your E2E Studios website. Use the form below to login. If you require support please get in touch at <a href="https://e2estudios.com/contact">https://e2estudios.com/contact</a>.</p>';
}
add_filter('login_message', 'login_message');

add_filter( 'login_headerurl', 'custom_loginlogo_url' );

function custom_loginlogo_url($url) {

     return 'https://e2estudios.com';

}








?>
